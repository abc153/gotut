package main

import "fmt"

func foo(c chan int, someValue int) {
	c <- someValue * 5
}

func main() {
	fooval := make(chan int) //fooval is a channel of int type

	go foo(fooval, 5)
	go foo(fooval, 3)

	//v1 := <- fooval
	//	v2 := <- fooval
	v1, v2 := <-fooval, <-fooval

	fmt.Println(v1, v2)

}
