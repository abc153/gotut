// package main

// import "fmt"

// type Vehicle interface {
// 	Structure() []string // Common Method in different interface
// 	Speed() string
// }

// type Human interface {
// 	Structure() []string // Common Method in different interface
// 	Performance() string
// }

// type Car string

// func (c Car) Structure() []string {
// 	var parts = []string{"ECU", "Engine", "Air Filters", "Wipers", "Gas Task"}
// 	return parts
// }

// func (c Car) Speed() string {
// 	return "200 Km/Hrs"
// }

// type Man string

// func (m Man) Structure() []string {
// 	var parts = []string{"Brain", "Heart", "Nose", "Eyelashes", "Stomach"}
// 	return parts
// }

// func (m Man) Performance() string {
// 	return "8 Hrs/Day"
// }

// func main() {
// 	var bmw Vehicle
// 	bmw = Car("World Top Brand")

// 	var labour Human
// 	labour = Man("Software Developer")

// 	for i, j := range bmw.Structure() {
// 		fmt.Printf("%-15s <=====> %15s\n", j, labour.Structure()[i])
// 	}
// }

package main

import "fmt"

// Interface 1
type AuthorDetails interface {
	details()
}

// Interface 2
type AuthorArticles interface {
	articles()
}

// Structure
type author struct {
	a_name    string
	branch    string
	college   string
	year      int
	salary    int
	particles int
	tarticles int
}

// Implementing method
// of the interface 1
func (a author) details() {

	fmt.Printf("Author Name: %s", a.a_name)
	fmt.Printf("\nBranch: %s and passing year: %d", a.branch, a.year)
	fmt.Printf("\nCollege Name: %s", a.college)
	fmt.Printf("\nSalary: %d", a.salary)
	fmt.Printf("\nPublished articles: %d", a.particles)

}

// Implementing method
// of the interface 2
func (a author) articles() {

	pendingarticles := a.tarticles - a.particles
	fmt.Printf("\nPending articles: %d", pendingarticles)
}

// Main value
func main() {

	// Assigning values
	// to the structure
	values := author{
		a_name:    "Mickey",
		branch:    "Computer science",
		college:   "XYZ",
		year:      2012,
		salary:    50000,
		particles: 209,
		tarticles: 309,
	}

	// Accessing the method
	// of the interface 1
	var i1 AuthorDetails = values
	i1.details()

	// Accessing the method
	// of the interface 2
	var i2 AuthorArticles = values
	i2.articles()

}
