// // using package bufio
// package main

// import (
// 	"bufio"
// 	"fmt"
// 	"os"

// 	//strconv is used to convert string to num
// 	"strconv"
// )

// func main() {

// 	scanner := bufio.NewScanner(os.Stdin)
// 	fmt.Printf("Type the year you were born: ")
// 	scanner.Scan()
// 	input, _ := strconv.ParseInt(scanner.Text(), 10, 64)
// 	fmt.Printf("You will be %2d years old at the end of 2021!", 2021-input)
// }

// package main *************************************************************************************************

// import (
// 	"fmt"
// 	"math"
// )

// type Vertex struct {
// 	X, Y float64
// }

// func Abs(v Vertex) float64 {
// 	return math.Sqrt(v.X*v.X + v.Y*v.Y)
// }

// func main() {
// 	v := Vertex{3, 4}
// 	fmt.Println(Abs(v))
// }

// package main*****************************************GOROUTINE/Waitgroup****************

// import (
// 	"fmt"
// 	"sync"
// 	"time"
// )

// var wg sync.WaitGroup

// func say(s string) {
// 	for i := 0; i < 3; i++ {
// 		fmt.Println(s)
// 		time.Sleep(time.Second * 1)
// 	}
// 	wg.Done()
// }

// func main() {
// 	wg.Add(1)
// 	go say("Hey")
// 	wg.Add(1)
// 	go say("There")
// 	wg.Wait()
// }

package main

import "fmt"

func foo(c chan int, someValue int) {
	c <- someValue * 5
}

func main() {
	fooval := make(chan int) //fooval is a channel of int type

	go foo(fooval, 5)
	go foo(fooval, 3)

	//v1 := <- fooval
	//	v2 := <- fooval
	v1, v2 := <-fooval, <-fooval

	fmt.Println(v1, v2)

}
