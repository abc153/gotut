package main

import (

	// log errors

	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

//Book Struct(Model)
type Book struct {
	ID     string  `json:"id"`
	Isbn   string  `json:"isbn"`
	Title  string  `json:"title"`
	Author *Author `json:"author"` //it will have its own struct
}

//Author struct
type Author struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

//Init books var as a slice Book struct
var books []Book

//Get all Books
func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(books)
}

//Get single book
func getBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) //Get Params
	//	Loop through books and find with id
	for _, item := range books {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			break
		}
	}
	//json.NewEncoder(w).Encode(&Book{})
}

//Create a new book
func createBook(w http.ResponseWriter, r *http.Request) {

}

//Update the book
func updateBook(w http.ResponseWriter, r *http.Request) {

}

//Delete the book
func deleteBook(w http.ResponseWriter, r *http.Request) {

}

func main() {
	//Init router (mux router)
	r := mux.NewRouter()

	//Mock data - @todo - implement DB
	books = append(books, Book{ID: "1", Isbn: "44786", Title: "First Book", Author: &Author{Firstname: "Rob", Lastname: "Doe"}})
	books = append(books, Book{ID: "2", Isbn: "14146", Title: "Second Book", Author: &Author{Firstname: "Smith", Lastname: "Steve"}})

	// Create route handlers which will establish endpoints for our api
	r.HandleFunc("/api/books", getBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", getBook).Methods("GET")
	r.HandleFunc("/api/books", createBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")

	//to run the server
	//using log.fatal that way if it fails we get an error
	log.Fatal(http.ListenAndServe(":8000", r))

}
